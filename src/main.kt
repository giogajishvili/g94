import kotlin.math.sqrt
import kotlin.math.pow
class Point(var numenator: Int, var denumenator: Int ) {
    override fun toString(): String {
        return("$numenator, $denumenator")
    }
    override fun equals(other: Any?): Boolean {
        if(other is Point) {
            if(numenator == other.numenator && denumenator == other.denumenator) {
                return true
            }
        }
        return false
    }
    fun toSymmetry(): Point {
        val clone = Point(numenator, denumenator)
        clone.numenator = -numenator
        clone.denumenator = -denumenator
        return result
    }
    fun calcDistance(anotherPoint: Point): Double {
        return sqrt((anotherPoint.numenator - numenator).toDouble().pow(2) +
                (anotherPoint.denumenator - denumenator).toDouble().pow(2))
    }
}
fun main() {
    var obj = Point(5,7)
    var obj2 = Point(-8,9)
    var f1 = Fraction(9.0,12.0)
    var f2 = Fraction(3.0,4.0)
}
// 2
class Fraction(var numerator:Double = 0.0, var denominator:Double = 0.0) {
    override fun equals(other: Any?): Boolean {
        if(other is Fraction) {
            if(numerator / denominator == other.numerator / other.denominator) {
                return true
            }
        }
        return false

    }
    fun reduceFraction(num: Double, returnNum: Boolean): Any {
        if(returnNum) {
            return (numerator / num) / (denominator / num)
        }
        val clone = Fraction(numerator, denominator)
        clone.numerator /= num
        clone.denominator /= num
        return clone
    }
    fun subtractFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) - (anotherFraction.numerator /
                anotherFraction.denominator)
    }
    fun addFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) + (anotherFraction.numerator /
                anotherFraction.denominator)
    }
    fun divideFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) / (anotherFraction.numerator /
                anotherFraction.denominator)
    }
    fun multiplyFraction(anotherFraction: Fraction ): Double {
        return (numerator / denominator) * (anotherFraction.numerator /
                anotherFraction.denominator)
    }
}
